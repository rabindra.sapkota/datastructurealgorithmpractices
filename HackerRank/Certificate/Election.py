# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 16:39:18 2020

@author: rabindra
"""
'''
Votes array represents array of vote on the election(array of party name). For a 
party to be elected it should have at least five percent of total vote. Write a
function to return array of elected parties. Returned array has to be sorted 
alphabetically
'''

def parliamentParties(votes):
    dic = dict()
    
    for vote in votes:
        if vote in dic.keys():
            dic[vote] += 1
        else:
            dic[vote] = 1
            
    elected_parties = list()
    total_votes = sum(dic.values())
    
    for party,vote in dic.items():
        if vote / total_votes > 0.05:
            elected_parties.append(party)
    
    elected_parties.sort()
    return elected_parties
    
print(parliamentParties(['Congress','BJP','AM']))