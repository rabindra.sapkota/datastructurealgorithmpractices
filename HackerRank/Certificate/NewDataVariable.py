# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 15:17:53 2020

@author: rabindra
"""
"""
Create a new variable type called multiset. In this variable we have to be able to perform
tasks such as adding element, removing element, calculate length and use containment 
operation

Note: existing opearators can be operloaded using __OverloadType__

Example : len() using __len__ , if i in variable using __contains__ , print() using __repr__ (it has to return string)

"""



class Multiset:
    
    def __init__(self):
        self.multiset = []

    def add(self, val):
        self.multiset.append(val)
        pass

    def remove(self, val):
        self.multiset.remove(val)
        pass
        
    def __contains__(self, val):
        if val in self.multiset:
            return True
        else:
            return False
    
    def __len__(self):
        return len(self.multiset)
    
m = Multiset()
print(1 in m)
m.add(1)
print(1 in m)
m.remove(1)
print(1 in m)
m.add(2)
m.add(2)
print(len(m))
print(2 in m)
m.remove(2)
m.remove(2)
print(2 in m)
print(len(m))
print(m)

'''
q = int(input())
operations = []
for _ in range(q):
    operations.append(input())
    result = performOperations(operations)
'''