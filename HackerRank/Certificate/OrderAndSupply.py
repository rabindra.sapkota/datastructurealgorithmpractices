# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 16:28:47 2020

@author: rabindra
"""
'''Order and Supply. Order of quantity are represented in terms of array of order.
Stock available are represented in terms of integer k. Find count of total order
that can be supplied'''

def filledOrders(order, k):
    count = 0
    order.sort()
    length = len(order)
    
    for i in range(length):
        if order[i] <= k:
            count += 1
            k -= order[i]
        else:
            break
            
    return count

print(filledOrders([5,4,6], 3))
print(filledOrders([10,30], 40))