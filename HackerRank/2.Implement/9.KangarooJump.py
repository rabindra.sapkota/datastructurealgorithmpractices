# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 20:31:17 2020

@author: rabindra
"""

#ProblemStatement

'''
You are choreographing a circus show with various animals. For one act, you are given two 
kangaroos on a number line ready to jump in the positive direction 
(i.e, toward positive infinity).

- The first kangaroo starts at location x1 and moves at a rate of v1 meters per jump.
- The second kangaroo starts at location x2 and moves at a rate of v2 meters per jump.

You have to figure out a way to get both kangaroos at the same location at the same time 
as part of the show. Time taken for jump is same for both kangaroo.If it is possible,
return YES, otherwise return NO.

For example, kangaroo 1 starts at x1=2 with a jump distance v1=1 and kangaroo 2 starts at 
x2=1 and with a distance of jump v2=2.After one jump, they are both at x=3, x1+v1=2+1,
x2+v2=2+1), so our answer is YES.

Sample Input        Sample Output
0 3 4 2                 YES
0 2 5 3                 NO
0 2 100000000 1         YES
1 3 100000000 1         NO

'''

#SolutionDiscussion

""" More Complexity Involved on first case as time lap is infinite
and match can also occut at t = 1000000000000 . So second approach is taken.


def kangaroo(x1, v1, x2, v2):

    possiblity = "NO"

    if v1 > v2 and x1 <= x2:
        while x1 <= x2:
            if x1 == x2:
                possiblity = "YES"
          
            x1 = x1 + v1
            x2 = x2 + v2

    elif v1 < v2 and x1 >= x2:
        while x1 > x2:
            if x1 == x2:
                possiblity = "YES"
                
            x1 = x1 + v1
            x2 = x2 + v2

    elif v1 == v2 and x1 == x2:
        possiblity = "YES"

    print(possiblity)

kangaroo(0, 2, 1000000000000, 1)
#Would Take infinite time
#So do maths here

Let distance travelled by kangaroo1 and kangaroo2 at jump t=t be xn1 and xn2.So,

xn1 = x1 + v1*t    and xn2 = x2 + v2*t

for distance travelled at jump t has to be same, we should have :
    xn1 = xn2
or  x1 + v1*t = x2 + v2*t
or  x1 - x2 = (v2 - v1)*t
or t = (x1 - x2) / (v2-v1)

since t is mesured is second, For equality to be valid: 
 1. t must to be perfect integer
 2. It has to be positive(zero is also valid as for t=0 they will be at same place
    regardless of valocity) 

 
Note: Division by zero can occur

"""

def kangaroo(x1,v1,x2,v2):
    
    #To Avoid Division By Zero Error 
    if v1 != v2:
        time = (x1 - x2) / (v2 - v1)
        perfect_int = ( time == time*2 // 2)
    else:
        perfect_int = False
        time = -3
        #If velocity is same t is infinite so perfect_int is set as false
        #If velocity is same t is infinite so it is set as negative number
        
    if (time >= 0 and perfect_int):
        probabilty = "YES"
    else:
        probabilty = "NO"
    
    print(probabilty)
    return(probabilty)



kangaroo(1, 3, 100000, 1)

#Now faster as complexity is O(1)
#Math is really powerful