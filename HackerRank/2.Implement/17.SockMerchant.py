# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 14:49:43 2020

@author: rabindra
"""


'''
John works at clothing center. He has a large pile 
of sicks that he must pair by color for sale. Given an 
array of integer representing color of each sock, determine
how many pair of matching colors there are. Example there are
n = 7 socks with color ar = [1,2,1,2,1,3,2] . here is one pair
of color 1 and one pair of color 2. There are 3 odd socks left
so to total pair is 2
'''

def sockMerchant(n, ar):
    paired_socks = 0
    hash_table = set()
    
    for sock in ar:
        if sock in hash_table:
            paired_socks += 1
            hash_table.discard(sock)
        else:
            hash_table.add(sock)
        
    return paired_socks

user_input = list(map(int,input("Enter socks separated by comma: ").split(',')))
print (sockMerchant(len(user_input),user_input))