# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 21:42:21 2020

@author: rabindra
"""


"""
HackerLand University has the following grading policy:

Every student receives a grade in the inclusive range from 0 to 100.
Any grade less than 40 is a failing grade.
Sam is a professor at the university and likes to round each student's grade according to 
these rules:

1. If the difference between the grade and the next multiple of 5 is less than 3, round grade
    up to the next multiple of 5.
2. If the value of grade is less than 38, no rounding occurs as the result will still be a failing grade.

For example, grade = 84 will be rounded to 85 but grade = 29 will not be rounded because the
rounding would result in a number that is less than 40.

Given the initial value of grade for each of Sam's n students, write code to automate the 
rounding process. Function expects integer array and returns integer array

Examples:
    original_mark   rounded_marks
        73              75
        67              67
        38              40
        33              33

"""

def gradingStudents(grades):
    return [grade + 5 - grade % 5 if ((grade % 5) > 2 and grade > 37) else grade for grade in grades]

user_input = [73,67,38,33]
print(gradingStudents(user_input))

#Expression after match also could be (grade//5 + 1)*5
