# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 13:26:53 2020

@author: rabindra
"""


'''
Marie invented a Time Machine and wants to test it by time-traveling to visit Russia on the Day
of the Programmer (the 256th day of the year) during a year in the inclusive range from
1700 to 2700.

From 1700 to 1917, Russia's official calendar was the Julian calendar; since 1999 they used 
the Gregorian calendar system. The transition from the Julian to Gregorian calendar system
occurred in 1918, when the next day after January 31st was February 14th. This means that in
1918, February 14 was the 31st day of the year in Russia.

In both calendar systems, February is the only month with a variable amount of days;it has 29
days during a leap year, and 28 days during all other years. In the Julian calendar, leap years
are divisible by 4; in the Gregorian calendar, leap years are either of the following:

1. Divisible by 400.
2. Divisible by 4 and not divisible by 100.

Given a year, y, find the date of the 256th day of that year according to the official Russian 
calendar during that year. Then print it in the format dd.mm.yyyy, where dd is the two-digit 
day, mm is the two-digit month, and yyyy is y.

For example, the given year = 1984. 1984 is divisible by 4, so it is a leap year. The 256th
day of a leap year after 1918 is September 12, so the answer is 12.09.1984.

Function receives integer denoting year
'''
def russian_leap_year(year):
    if year < 1918 and year % 4 == 0:
        return True
    elif year < 1918 and year % 4 != 0:
        return False
    elif year > 1918 and year % 400 == 0:
        return True
    elif year > 1918 and year % 4 == 0 and year % 100 != 0:
        return True
    else:
        return False
    
#256th day for leap year is september 12 and non-leap year is september 13
#Since transition is in year 1918 handle it separately
        
def dayOfProgrammer(year):
    if russian_leap_year(year) and year != 1918:
        return '12.09.'+str(year)
    elif not russian_leap_year(year) and year != 1918:
        return '13.09.'+str(year)
    else:
        return '29.09.1918'
    
user_input = int(input("Enter Year: "))
print(dayOfProgrammer(user_input))