# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 19:58:03 2020

@author: rabindra
"""

"""
Divisible Sum Pair

You are given an array of n integers as arr , and a positive integer k. Find and print the number of
(i,j) pairs where i<j and ar[i] + ar[j] is divisible by k.

Example : for an array arr[1,3,2,6,1,2] when k = 3. Valid pair with i<j and sum divisible by k are:
    
(0,2) => ar[0] + ar[2] = 1 + 2 = 3
(0,5) => ar[0] + ar[5] = 1 + 2 = 3
(1,3) => ar[1] + ar[3] = 3 + 6 = 9
(2,4) => ar[2] + ar[4] = 2 + 1 = 3
(4,5) => ar[4] + ar[5] = 1 + 2 = 3

Write function which accepts array,array_length and k. It should provide count of
total cases where mentioned condition is matched
"""

def divisibleSumPairs(n, k, ar):
    count = 0
    for i in range(n):
        for j in range(i+1,n):
            if (ar[i] + ar[j]) % k == 0:
               count += 1
    
    return count

k = 3
arr = list(map(int,input("Enter Number separated by comma: ").split(',')))
n = len(arr)

print(divisibleSumPairs(n, k, arr))