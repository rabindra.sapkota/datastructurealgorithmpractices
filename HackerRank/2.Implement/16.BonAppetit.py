# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 14:35:48 2020

@author: rabindra
"""
'''
Anna and Brian are sharing a meal at a restuarant and they agree to split the bill equally. 
Brian wants to order something that Anna is allergic to though, and they agree that Anna won't
pay for that item. Brian gets the check and calculates Anna's portion. You must determine if 
his calculation is correct. If correct print Bon Appetit  else print integer amount of money 
that Brian owes Anna

bill is an array of ordered items, k in integer in zero based intex for food anna didn't eat
b is anna's contribution to billl
'''

def bonAppetit(bill, k, b):
    total_bill = sum(bill)
    bill_without_algeric = total_bill - bill[k]
    
    if bill_without_algeric / 2 == b:
        print("Bon Appetit")
        return "Bon Appetit"
    else:
        print(int(bill[k] / 2))
        return int(bill[k]/2)
    


print(bonAppetit([3,10,2,9], 1, 7))