# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 17:26:31 2020

@author: rabindra
"""

"""
Lily has a chocolate bar that she wants to share it with Ron for his birthday. Each of the squares has an integer on it. 
She decides to share a contiguous segment of the bar selected such that the length of the segment matches Ron's birth month 
and the sum of the integers on the squares is equal to his birth day. You must determine how many ways she can divide the chocolate.

let her chocolate has a sequence [5,3,2,4,4,1,5]. let month be 3 and day by 10 i.e March10
Here we have to find 3 continuous element whose sum is 10.
sum(s[0:4]), sum(s[2:6]) , sum(s[4:10]) have sum 10 on continuous length of 3

Thus she can distribute chocolate on 3 ways

Write function as
def birthday(s,d,m):
    
    return count
    
where s is array string, d is day of birth and m is month of month. Count is no of way she can distribute chocolate
"""


'''  Insight

On adding or subtracting on continuous element , prefix sum algorithm is useful
helps to reduce complexity to linear. Finding continuous element whose sum is 10
Focus on word continuous. Prefix sum applicable only on continuous elements
'''

def prefarray(arr):
    length = len(arr)
    
    for i in range(1,length):
        arr[i] = arr[i] + arr[i-1]
    
    return arr

#m is length on which sum is to be checked
#d is total sum on n continuous integer. i.e if sum if m continuous element equals d
#s is array of integer
def birthday(s, d, m):
    prefixed = prefarray(s)
    length = len(s)
    count = 0
    prev = 0

    for i in range(m-1,length):
        print(prefixed[i],prev,d)
        
        if prefixed[i] - prev == d:
            count += 1
            
        prev = prefixed[i - m + 1]
        
    return count,prefixed

day = int(input("Enter day of birth: "))
month = int(input("Enter Month of birth"))
user_input = list(map(int,input("Enter num sep by comma: ").split(',')))

print(birthday(user_input, day, month))