# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 17:14:08 2020

@author: rabindra
"""

'''
Maria plays college basketball and wants to go pro. Each season she maintains a record of her play. She tabulates the number of times she breaks her season record for most points and least points in a game. Points scored in the first game establish her record for the season, and she begins counting from there.

For example, assume her scores for the season are represented in the array 
scores = [12,24,10,24]

At first game there is no break of recod so her lowest and highest score is first score
at second game her best score become 24 and wrost is still 12 thus best score is beaten once. At third game
her wrost record is breaken so best = 24 , wrost = 10 and both wrost break becomes 1.
On final game  neither her best nor wrost score is broken. Since it is end of scores
She breaks her best one time and wrost one time thus it should return 1,1
interger containing most_beak and least_record_break value
'''

def breakingRecords(scores):
    min_score = scores[0]
    max_score = scores[0]
    min_break = 0
    max_break = 0
    length = len(scores)
    
    for i in range(1,length):
        if scores[i] > max_score:
            max_score = scores[i]
            max_break += 1
            
        if scores[i] < min_score:
            min_score = scores[i]
            min_break += 1
    
    return max_break,min_break

print(breakingRecords([5,5,6,9,4]))