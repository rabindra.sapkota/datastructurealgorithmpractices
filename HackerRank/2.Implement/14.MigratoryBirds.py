# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 12:54:18 2020

@author: rabindra
"""


'''
You have been asked to help study the population of birds migrating across the continent. Each
type of bird you are interested in will be identified by an integer value. Each time a particular kind of bird is 
spotted, its id number will be added to your array of sightings. You would like to be able to
find out which type of bird is most common given a list of sightings. Your task is to print
the type number of that bird and if two or more types of birds are equally common, choose 
the type with the smallest ID number.

Constraints : 5 < n < 2*(10^5)     Each type is 1,2,3,4 or 5
'''

def migratoryBirds(arr):
    dic = dict({'1':0,'2':0,'3':0,'4':0,'5':0})
    
    for birds in arr:
        dic[str(birds)] += 1
        
    more_common_value = max(dic.values())
    more_common_bird = 6
    
    for key in dic.keys():
        if dic[key] == more_common_value and int(key) < more_common_bird:
            more_common_bird = int(key)
    
    return more_common_bird


user_input = list(map(int,input("Enter Number Separated by comma: ").split(',')))
print(migratoryBirds(user_input))