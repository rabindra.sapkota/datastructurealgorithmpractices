# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 16:57:04 2020

@author: rabindra  I loved it
"""
'''
You will be given two arrays of integers and asked to determine all integers that satisfy the following two conditions:

1. The elements of the first array are all factors of the integer being considered
2. The integer being considered is a factor of all elements of the second array

These numbers are referred to as being between the two arrays. You must determine how many such numbers exist.


Function Description
Complete the getTotalX function in the editor below. It should return the number of integers that are betwen the sets.

getTotalX has the following parameter(s):

1. a: an array of integers
2. b: an array of integers


Sample Input: a = [ 2, 4] and b [16,32,96]
Sample Output: 3


Explanation
2 and 4 divide evenly into 4, 8, 12 and 16. i.e these numbers are divided by both 2 and 4
4, 8 and 16 divide evenly into 16, 32, 96.  i.e these numbers all 16,32,96

4, 8 and 16 are the only three numbers for which each element of a is a factor and each is a factor of all elements of b. 
'''

def getTotalX(a, b):
    def gcd(min_num,max_num):
        if min_num > max_num:
            min_num , max_num = max_num , min_num
        
        while max_num % min_num != 0:
            gcd_check = max_num % min_num
            max_num = min_num
            min_num = gcd_check
        
        return min_num
        

    def gcd_list(number_list):
        gcd_list_value = number_list[0]
        length = len(number_list)
    
        for i in range(1,length):
            gcd_list_value = gcd(gcd_list_value, number_list[i])
        
        return gcd_list_value


    def lcm_list(number_list):
        lcm_list_value = number_list[0]
        length = len(number_list)
    
        for i in range(1,length):
            lcm_list_value = lcm_list_value * number_list[i] / gcd(lcm_list_value, number_list[i])
        
        return int(lcm_list_value)

    min_limit = lcm_list(a)
    max_limit = gcd_list(b)
    count = 0
    count_inc = min_limit

    while min_limit <= max_limit:
        if max_limit % min_limit == 0:
            count += 1
            
        min_limit = min_limit + count_inc
    
    return count

print(getTotalX([2,4], [16,32,96]))