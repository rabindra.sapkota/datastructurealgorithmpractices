# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 22:43:25 2020

@author: rabindra
"""

"""

Sam's house has an apple tree and an orange tree that yield an abundance of fruit. 
On graph s is the start point, and t  is the endpoint. The apple tree is to the left 
of his house, and the orange tree is to its right. You can assume the trees are
located on a single point, where the apple tree is at point a, and the orange tree is at
point b.

When a fruit falls from its tree, it lands d units of distance from its tree of origin 
along the x-axis. A negative value of d means the fruit fell d units to the tree's left, and
a positive value of d means it falls d units to the tree's right.

Given the value of d for m apples and n oranges, determine how many apples and oranges will 
fall on Sam's house (i.e., in the inclusive range [s,t] )?

apples: integer array, distances at which each apple falls from the tree.
oranges: integer array, distances at which each orange falls from the tree.

Draw Picture on copy/other tool. Visualiztion problem is understanding problem

"""




def countApplesAndOranges(s, t, a, b, apples, oranges):
    apple_in_house = 0
    orange_in_house = 0

    for orange in oranges:
        if (orange + b) >= s and (orange + b) <= t:
            orange_in_house += 1

    for apple in apples:
        if (apple + a) >=s and (apple + a) <= t:
            apple_in_house += 1
    print(apple_in_house)
    print(orange_in_house)