# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 17:21:09 2020

@author: rabindra
"""

"""
Given a square matrix, calculate the absolute difference between the sums of its diagonals.
Input is 2D array
                    1   2   4
Example : If A =    3   3   6
                    5   2   1
                    
Its left diagonal is 1      
                        3
                            1


And right is                4
                        3
                    5
                    
So output should be | (1+3+1) - (5+3+4) |

"""

def diagonalDifference(arr):

    left_sum = 0
    right_sum = 0
    length = len(arr)

    for i in range(length):
        left_sum += arr[i][i]
        right_sum += arr[i][length-i-1]

    return abs(left_sum - right_sum)

print(diagonalDifference([[1,2,3],[1,6,2],[2,5,6]]))