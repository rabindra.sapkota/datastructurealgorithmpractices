# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 19:48:22 2020

@author: rabindra
"""


"""
Given a time in 12-hour AM/PM format, convert it to military (24-hour) time.

Note: Midnight is 12:00:00AM on a 12-hour clock, and 00:00:00 on a 24-hour clock. 
Noon is 12:00:00PM on a 12-hour clock, and 12:00:00 on a 24-hour clock.

Function Description
Complete the timeConversion function in the editor below. It should return a new string
representing the input time in 24 hour format.

timeConversion has the following parameter(s):

s: a string representing time in 12 hour format

Sample Input  : 07:05:45PM
Sample Output : 19:05:45

"""


def timeConversion(s):
    
    is_pm = s[-2:].upper == 'PM'
    time_list = list(map(int, s[:-2].split(':')) )
    
    if is_pm and time_list[0] < 12 :
        time_list[0] += 12
        
    if not is_pm and time_list[0] == 12 :
        time_list[0] = 0
        
    return (":".join(map(lambda x:str(x).rjust(2,'0'),time_list)))

user_input = input("Enter time: ")
print(timeConversion(user_input))


"""
def timeConversion(s):
    
    if s[-2:].upper() == 'AM':
       
        if s[0:2] == '12':
            return '00' + s[2:-2]
        else:
            return s[:-2]

    
    elif s[-2:].upper() == 'PM':
        
        if s[0:2] == '12':
            return s[:-2]
        else:
            hour_part = s[0:2]
            new_hour_part = 12 + int(hour_part)
            return str(new_hour_part) + s[2:-2]
    
user_input = input("Enter time: ")
print(timeConversion(user_input))


 #12 baje le lyang garyo teslai handle arna parcha

"""






"""
import re

time = raw_input()

#(?P<name>regex) captures matching regex group and assigns to a variable

times = re.search(r"(?P<hh>..):(?P<mm>..):(?P<ss>..)(?P<ampm>..)", time)
hour = times.group('hh')

if times.group('ampm') == 'PM':
	if hour == '12':
		hr = '12'
	else:
		hr = str(int(hour) + 12)
else:
    if hour == '12':
        hr = '00'
    else:
        hr = hour

print hr + ':' + times.group('mm') + ':' + times.group('ss')

"""