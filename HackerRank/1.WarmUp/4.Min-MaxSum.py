# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 19:30:12 2020

@author: rabindra
"""


"""
Given five positive integers, find the minimum and maximum values that can be calculated by 
summing exactly four of the five integers. Then print the respective minimum and maximum 
values as a single line of two space-separated long integers.

For example, arr= [1,3,5,7,9] . Our minimum sum is 1+3+5+7 =16 and our maximum 
sum is 3+5+7+9=24. We would print 16 24

"""

def miniMaxSum(arr):
    
    mini = min(arr)
    maxi = max(arr)
    sumi = sum(arr)
    
    print(sumi - maxi , sumi - mini)
    
    
"""
or alternatively as

def minMaxSum(arr):
    
    mini = 0
    maxi = 0
    sumi = 0
    
    for value in arr:
        sumi += value
        
        if value < mini:
            mini = value
        elif value > maxi:
            maxi = value
    
    print(sumi - maxi, sumi - mini)


"""