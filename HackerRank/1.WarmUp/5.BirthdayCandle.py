# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 19:41:00 2020

@author: rabindra
"""


"""
You are in charge of the cake for your niece's birthday and have decided the cake will have 
one candle for each year of her total age. When she blows out the candles, she’ll only be 
able to blow out the tallest ones. Your task is to find out how many candles she can 
successfully blow out.

For example, if your niece is turning  years old, and the cake will have 4 candles of 
height 4, 4, 1, 3, she will be able to blow out 2 candles successfully, since the tallest 
candles are of height  4 and there are 2 such candles.


Function Description

Complete the function birthdayCakeCandles in the editor below. It must return an integer 
representing the number of candles she can blow out. birthdayCakeCandles has the following 
parameter(s):

ar: an array of integers representing candle heights


"""

def birthdayCakeCandles(ar):
    
    max_value = max(ar)
    return ar.count(max_value)

user_input = input("Enter Candles: ")
print(birthdayCakeCandles(user_input))




"""
n = int(input())
height_i = [int(x) for x in input().split(' ')]

max_num, max_count = 0, 0
for height in height_i:
    if height == max_num:
        max_count += 1
    elif height > max_num:
        max_num = height
        max_count = 1
print(max_count)

"""