# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 17:02:21 2020

@author: rabindra
"""


"""
Alice and Bob each created one problem for HackerRank. A reviewer rates the two challenges, awarding points on a scale from  to  for three categories: problem clarity, originality, and difficulty.

We define the rating for Alice's challenge to be the triplet a=(a[0],a[1],a[2]), and the rating for Bob's challenge to be the triplet b=(b[0],b[1],b[2]).

Your task is to find their comparison points by comparing a[0] with b[0], a[1] with b[1], and a[2] with b[2].

If a[i] > b[i], then Alice is awarded  point.
If a[i] < b[i], then Bob is awarded  point.
If a[i] = b[i], then neither person receives a point.

Comparison points is the total points a person earned.

Example : if a = [5,6,7] and b = [3,6,10] then it has to return [1,1]

as bob obtain 1 point in total and alice obtain 1 point in total

"""

def compareTriplets(a, b):
    alice_point_list = [i>j for i,j in zip(a,b)]
    bob_point_list = [i>j for i,j in zip(a,b)]
    return(sum(alice_point_list),sum(bob_point_list))



'''
Solution : 2

def compareTriplets(a, b):
    alice_score = 0
    bob_score = 0
    
    for i,j in zip(a,b):
        if i > j:
            alice_score += 1
        elif j > i:
            bob_score += 1
    return(alice_score,bob_score)

Trick is : Rather than summping at last why not sum while iterating loop

'''