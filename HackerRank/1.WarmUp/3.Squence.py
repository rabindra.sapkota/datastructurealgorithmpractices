# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 18:04:23 2020

@author: rabindra
"""
"""
For say n= 4, print symbol in form

   #
  ##
 ###
####

Notice here row and column are in square form with space gradually decreasing
Write function to print this sequence where it has to receive n as input paramenter



"""

def printp(n):
    
    for i in range(n):
        sym = '#'*(i+1)
        print (sym.rjust(n))
    
user_input = int(input("Enter no of peace: "))
printp(user_input)