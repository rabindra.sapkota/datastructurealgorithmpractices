# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 21:47:01 2020

@author: rabindra
"""


import datetime
def fib(n):
    stack = [0] * (n + 1)
    
    if n == 0:
        return 1
    elif n == 1:
        return 1
    
    stack[0] = 1
    stack[1] = 1
    
    for i in range(2,n+1):
        stack[i] = stack[i-1] + stack[i-2]
    
    return stack[n]

print(datetime.datetime.now())
print(fib(40))
print(datetime.datetime.now())