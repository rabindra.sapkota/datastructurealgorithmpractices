# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 22:04:16 2020

@author: rabindra
"""

import datetime
import sys
sys.setrecursionlimit(150000)

def fib(n):
    if n == 0:
        return 1
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

print(datetime.datetime.now())
print(fib(40))
print(datetime.datetime.now())