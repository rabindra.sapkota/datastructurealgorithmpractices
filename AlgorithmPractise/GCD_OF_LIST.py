# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 16:19:29 2020

@author: rabindra
"""
''' Function to return LCM and HCF of numbers in the list  '''

def gcd(min_num,max_num):
    if min_num > max_num:
        min_num , max_num = max_num , min_num
        
    while max_num % min_num != 0:
        gcd_check = max_num % min_num
        max_num = min_num
        min_num = gcd_check
        
    return min_num
        

def gcd_list(number_list):
    gcd_list_value = number_list[0]
    length = len(number_list)
    
    for i in range(1,length):
        gcd_list_value = gcd(gcd_list_value, number_list[i])
        
    return gcd_list_value


def lcm_list(number_list):
    lcm_list_value = number_list[0]
    length = len(number_list)
    
    for i in range(1,length):
        lcm_list_value = lcm_list_value * number_list[i] / gcd(lcm_list_value, number_list[i])
        
    return int(lcm_list_value)


ul = list(map(int,input("Number separated by comma : ").split(',')))
print(lcm_list(ul))
print(gcd_list(ul))