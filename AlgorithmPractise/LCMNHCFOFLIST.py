# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 22:06:07 2020

@author: rabindra
"""

def gcd(mini,maxi):
    if mini > maxi:
        mini , maxi = maxi , mini
    
    result = maxi % mini
    while result != 0:
        maxi = mini
        mini = result
        result = maxi % mini
        
    return mini

def lcm(mini,maxi):
    product = mini * maxi
    gcd_result = gcd(mini,maxi)
    return int(product / gcd_result)


ls = [12,18,24,25]
n = len(ls)
gcdl = ls[0]
for i in range(2,n-1):
    gcdl = gcd(gcdl,ls[i])

print(gcdl)