# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 11:24:42 2020

@author: rabindra
"""

#BadApproach As we aren't required to print values but count only
'''
def calc(begin,end):
    count = 0
    begin = begin + 1
    while begin < end:
        if begin % 2 == 0:
            count += 1
        begin += 1
      
    return count    
    
begin = int(input("Enter Begining: "))
end = int(input("Enter End:"))
print(calc(begin,end))

'''
#Complexity is linear

def calc(begin,end):
    if end % 2 == 0:
        end -= 1
    if begin % 2 == 0:
        begin += 1
    
    return int((end - begin) / 2)

begin = int(input("Enter Begining: "))
end = int(input("Enter End:"))
print(calc(begin,end))


