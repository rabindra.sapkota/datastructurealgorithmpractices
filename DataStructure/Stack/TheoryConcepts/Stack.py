# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 23:20:19 2020

@author: rabindra
"""


class stack:
    def __init__(self):
        self.data = list()
        self.length = 0
        
    def push(self,value):
        self.data.append(str(value))
        self.length += 1
        
    def __len__(self):
        return self.length
    
    def __repr__(self):
        return ' '.join(self.data)
    
    
    def __contains__(self,value):
        if value in self.data:
            return True
        return False
    
    def pop(self):
        if self.length == 0:
            print("Stack Is Empty")
            return
        popped_value = self.data.pop()
        self.length -= 1
        return popped_value
    

    
a = stack()
print(len(a))
a.push(3)
print(len(a))
a.push(4)
a.push(5)
a.push(7)
b = a.pop()
print(len(a))
print(a)
a.pop()
print(len(a))
a.pop()
a.pop()
a.pop()