# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 19:43:16 2020

@author: rabindra
"""


class Node:
    def __init__(self,value):
        self.data = value
        self.next = None
        
class LinkedList:
    def __init__(self):
        self.head = None
        self.length = 0

     #Storing at last will be O(n).So while push data is stored at first 
     #and while pop it is taken from first reducing comlexity to O(1)
    def push(self,value):
        new_node = Node(value)
        new_node.next = self.head
        self.head = new_node
        self.length += 1

    def __repr__(self):
        current_node = self.head
        if current_node is None:
            return "empty"
        data = []
        while current_node is not None:
            data.append(str(current_node.data))
            current_node = current_node.next
        return " ".join(data) + '|' 
    
    def pop(self):
        if self.length == 0:
            print("Stack is empty")
            return
        self.head = self.head.next
        self.length -= 1
    
    def is_empty(self):
        if self.length == 0:
            return True
        return False
    
    def top(self):
        if self.length == 0:
            return "EmptyStack"
        return self.head.data


l = LinkedList()
print(l)
l.push(3)
print(l)
l.push(4)
print(l.top())
l.push(5)
print(l.top())
print(l)
l.pop()
print(l)
l.pop()
print(l)
l.pop()
l.pop()
print(l.top())
