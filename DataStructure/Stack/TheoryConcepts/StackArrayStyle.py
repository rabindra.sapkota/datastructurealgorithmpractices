# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 09:52:40 2020

@author: rabindra
"""


# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 23:20:19 2020

@author: rabindra
"""


class stack:
    def __init__(self):
        self.data = [None]*32
        self.index = -1
        
    def push(self,value):
        if ( self.index + 1 ) < len(self.data):
            self.index += 1
            self.data[self.index] = value
            return
        print("Stack is full")
        
    def __len__(self):
        return self.index + 1
    
    def __repr__(self):
        stack_data = [str(self.data[index]) for index in range(self.index + 1)]
        return "|"+' '.join(stack_data)
        
    def __contains__(self,value):
        if value in self.data:
            return True
        return False
    
    def top(self):
        if self.index == -1:
            return "Empty Stack"
        return self.data[self.index]
    
    def pop(self):
        if self.index == -1:
            print("Stack Is Empty")
            return
        self.index -= 1
    
    
a = stack()
print("Length is " + str(len(a)))
print(a)
a.push(3)
print("Length is " + str(len(a)))
print(a)
a.push(4)
a.push('ram')
print(a.top())
a.push(7)
print("Length is " + str(len(a)))
print(a)
print(a.top())
a.pop()
print(a)
print(a.top())
print(3 in a)
a.pop()
#print(len(a))
a.pop()
print(a)
a.pop()
print(a)
a.pop()
print(a)
#a.pop()
#a.pop()