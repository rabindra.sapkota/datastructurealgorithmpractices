# -*- coding: utf-8 -*-
"""
Created on Sat May  2 22:23:32 2020

@author: rabindra
"""


# -*- coding: utf-8 -*-
"""
Created on Sat May  2 19:22:55 2020

@author: rabindra
"""


'''
Observations:
    1. Despite of type of notation (infix,prefix,postfix) , The order of operand is always same
    2. So operands can be appended on list w/o any comparision
    3. For appending order of precedence matters. For post fix if new encountered expression is 
       operator then pop from stack if element in stack has higher precendence(multiple pop possible)
       And push current operator
    4. Repeat process till end of infix expression
'''


def infix_to_post_fix(expression):
    precedence_dict = {'-':0,'+':1,'*':2,'/':3,'^':4}
    stack = []
    converted_expression = []
    
    #Check Precedence of operator and push/pop operation on stack
    def check_precedence_and_update(operator):
        stack_length = len(stack)
        last_stack_element = stack[stack_length - 1]
        #For empty stack no comparision is needed just push element
        if stack_length == 0 or last_stack_element == '(':
            stack.append(operator)
            return
        
        #If Higher precendence operator on stack then pop as it has to be calculated first else push operator
        if  precedence_dict[last_stack_element] >= precedence_dict[operator]:
            converted_expression.append(stack.pop())
            check_precedence_and_update(operator)
        else:
            stack.append(operator)
            
    def append_stack():
        for i in range(len(stack) -1 , -1 , -1):
            if stack[i] == '(':
                stack.pop()
                return
            converted_expression.append(stack.pop())
            

    for i in range(len(expression)):
        if expression[i] in precedence_dict.keys():
            check_precedence_and_update(expression[i])
            continue
        
        if expression[i] == '(':
            stack.append('(')
            continue
        
        if expression[i] == ')':
            append_stack()
            continue
        
        converted_expression.append(expression[i])
    
    #Expression like 2 + 3 , operator is pushed and whole expression is finished. So, operators in stacks have to be appended.
    for element in stack:
        converted_expression.append(element)
    return (''.join(converted_expression))


print(infix_to_post_fix('a*(b+c)'))