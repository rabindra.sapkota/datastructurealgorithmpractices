# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 00:12:16 2020

@author: rabindra
"""
'''
TheoryConcept
https://www.youtube.com/watch?v=jos1Flt21is&list=PL2_aWCzGMAwI3W_JlcBbtYTwiQSsOTa6P&index=19

Infix => <operand><operator><operand>  Eg: 2 + 3 , 2 * c, (7 - 3) * 5  ; operand can be constant, variable or expression itself
Prefix => <operator><operand><operand> Eg: + 2 3 , * 2 c, * 5 - 7 3 ; piala operate garna parne pachadi?? ; Also called polish notation
Postfix => <operand><operand><operator> Eg: 2 3 +, 2 c *, 5 7 3 - * ;paila operate garne paila ? also called reverse polish notation. Usful and intutive for programming

Conversion Steps:
1. Bracket use garera order of precendence follow garda easy hucha

To Prefix:
    a + b * c => a + (b * c) 
    first operate garna parne lai first convert so,
      a + (* b c)
  =>  + a (* b c)
  => + a * b c

To Postfix
  a + b * c => a + (b * c)
  first operate garna parne lai first convert so,
    a + (b c *)   #(b c * ) is treated as a single operator
=> a (b c *) +   # <FirstOperand><SecondOperand><Operator>
=> a b c * +

example : a * b + c * d -e i.e after with bracket for precedence expression is: 
    {(a*b) + (c * d)} - e

Prefix : {(* a b) + (* c d)} - e
   => {(* a b) (* c d) + } - e
   => - {+ (* a b) (* c d) } e 
   => - + * a b * c d e
   
Postfix : {(a b *) + (c d *)} - e
  => {(a b *) (c d *) + } - e   #Whole expression inside bracket is one operand. So first second operator
  => {(a b *) (c d *) + } e -
  => a b * c d * + e -
  
'''

"""
Write a function that evaluates postfix calculation, prefix calculation and gives result
"""

def evaluate_post_fix(expression):
    operand = {'+','-','*','/'}
    stack = []
    for value in expression:
        if value in operand:
            L1 = stack.pop()
            L2 = stack.pop()
            stack.append(perform_operation(value, L1, L2))
            continue
        stack.append(value)
    return stack.pop()

def perform_operation(operator,operand1,operand2):
    if operator == '+':
        return operand2 + operand1 
    #Division by zero is not handled. Handle it
    if operator == '-':
        return operand2 - operand1
    if operator == '*':
        return operand2 * operand1
    if operator == '/':
        return operand2 / operand1
    if operator == '^':
        return operand2 ** operand1

def evaluate_prefix(expression):
    operand = {'+','-','*','/'}
    stack = []
    for i  in range(len(expression)-1,-1,-1):
        if expression[i] in operand:
            L1 = stack.pop()
            L2 = stack.pop()
            stack.append(perform_operation(expression[i], L2, L1))
            continue
        stack.append(expression[i])
    return stack.pop()

post_expression = [4,3,'*',3,1,'*','+',2,'-']
pre_expression = ['-','+','*',4,3,'*',3,1,2]
#Assumes expression is valid
print(evaluate_post_fix(post_expression))
print(evaluate_prefix(pre_expression))