# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 19:24:02 2020

@author: rabindra
"""

"""
Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
An input string is valid if:
1) Open brackets must be closed by the same type of brackets.
2) Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.

Example 1:
    Input: "()"
    Output: true
Example 2:
    Input: "()[]{}"
    Output: true
Example 3:
    Input: "(]"
    Output: false
Example 4:
    Input: "([)]"
    Output: false
Example 5:
    Input: "{[]}"
    Output: true


"""
''' Dicationary is maintained with key as opening bracket and its
value as a closing bracket. user input is pushed to stack if it encounters
opening bracket. if it encounters closing bracket the stack is popped. The
poped value and current value has to match else it is invalid. On iterating
through whole list at final if stack is empty then it means all pair matched
so string is valid'''

def validate_bracket(user_input):
    stack = []
    for bracket in user_input:
        
        if bracket in validity_dic.keys():
            stack.append(bracket)
        
        else:
            
            if len(stack) == 0:
                return("Invalid")
            
            if validity_dic[stack.pop()] != bracket:
                return ("Invalid")

    if len(stack) == 0:
        return("Valid")
    else:
        return("Invalid")
    
validity_dic = {"(":")","{":"}","[":"]"}
user_input = input("Enter bracket to validate : ")
validate_result = validate_bracket(user_input)
print(validate_result)


#complexity six as per lizard.us I think(mn) where n is size of input and m is order of dictionary
