# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 23:04:37 2020

@author: rabindra
"""


def rev(string):
    stack = []
    length = len(string)
    #Fill up stack with charackers
    for charcter in string:
        stack.append(charcter)
    
    #PopValue to new list as assignment operation is invalid on python
    reverse_string = [None] * length
    for i in range(length):
        reverse_string[i] = stack.pop()        
    return ''.join(reverse_string)

print(rev("Rabindra"))

#Can also be done as s = rabindra ; print(s[::-1]) but knowledge of DS is useful
#Alternative approach can be using two variables i at begining at j at end. Run 
#Loop till i < j at swap characters at each position repetatively