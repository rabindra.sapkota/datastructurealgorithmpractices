# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 20:43:21 2020

@author: rabindra
"""


class Node:
    def __init__(self,value):
        self.data = value
        self.next = None
        self.prev = None
            
        
class DoublyLinkList:
    def __init__(self):
        self.head = None
        self.count = 0
        
    def addfirst(self,data):
        new_node = Node(data)
        first_node = self.head
        self.head = new_node
        new_node.prev = None
        
        if first_node is not None:
            new_node.next = first_node
            first_node.prev = new_node
        else:
            new_node.next = None
            
        self.count += 1
        
    #Over-riding print funtion. Return type must be string
    def __repr__(self):
        current_node = self.head
        totaldata = []
        while current_node is not None:
            totaldata.append(current_node.data)
            current_node = current_node.next
        if len(totaldata) == 0:
            return 'None <-> None'
        return 'None <-> '+' <-> '.join(totaldata)+' <-> None'
    
    def __len__(self):
        return self.count
    
    def addlast(self,data):
        if self.count == 0:
            self.addfirst(data)
        else:
            new_node = Node(data)
            current_node = self.head
            while current_node.next is not None:
                current_node = current_node.next
        
            current_node.next = new_node
            new_node.prev = current_node
            new_node.next = None
            self.count += 1
   
    #Pos is zero based position
    def addnode(self,data,pos):
        if pos == 0:
            self.addfirst(data)
        elif pos == self.count:
            self.addlast(data)
        elif pos > self.count:
            print("Invalid Position")
        else:
            new_node = Node(data)
            current_postion = 0
            current_node = self.head
            while current_postion + 1 != pos:
                current_node = current_node.next
                current_postion += 1
            temp_node = current_node.next  #Fri
            current_node.next = new_node    #mon.next =tue
            new_node.next = temp_node
            temp_node.prev = current_node
            self.count += 1
            
    def reverutil(self,prev_node,current_node):
        if current_node.next is None:
            prev_node.prev = current_node
            current_node.next = prev_node
            self.head = current_node
            return
        if prev_node is None:
            next_to_sort = current_node.next
            current_node.next = prev_node
            self.reverutil(current_node,next_to_sort)
            return
        
        next_to_sort = current_node.next
        current_node.next = prev_node
        prev_node.prev = current_node
        self.reverutil(current_node,next_to_sort)
            
    def reverse(self):
        self.reverutil(None,self.head)
        
    def __contains__(self,val):
        current_node = self.head
        while current_node is not None:
            if current_node.data == val:
                return True
            current_node = current_node.next
        return False
        
    def delnode(self,pos):
        if pos == 0:
            new_head = self.head.next
            self.head = new_head
            new_head.prev = None
            self.count -= 1
            return
        
        if pos < 0 or pos + 1 > self.count:
            print("Invalid Position")
            return
        
        current_pos = 0
        current_node = self.head
        while current_pos + 1 != pos:
            current_node = current_node.next
            current_pos += 1
        
        tmp_node = current_node.next.next
        current_node.next = tmp_node
        tmp_node.prev = current_node
        self.count -= 1


t = DoublyLinkList()
print(t)
t.addlast('fri')
print(t)
t.addfirst('sun')
print(t)
t.addlast('sat')
print(t)
t.addnode('mon', 1)
print(t)
t.addnode('tue',2)
print(t)
t.addnode('wed',3)
print(t)
t.addnode('thu',4)
print(t)
t.reverse()
print(t)
t.delnode(0)
print(t)
t.delnode(2)
print(t)
print(len(t))
print('Jan' in t)
print('sun' in t)