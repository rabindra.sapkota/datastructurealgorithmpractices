# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 21:50:46 2020

@author: rabindra
"""


# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 14:37:58 2020

@author: rabindra
"""

#Extra class variable tail is added to make insertion at first and last node
#in O(1). Useful if list has to be inserted at both first and last place frequently
class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return self.data

class LinkedList:
    def __init__(self):
        self.head = None
        self.length = 0
        self.tail = None
        
    def __repr__(self):
        node = self.head
        nodes = []
        while node is not None:
            nodes.append(node.data)
            node = node.next
        return " -> ".join(nodes)

    
    def __len__(self):
        return self.length

        
    def add_first(self,value):
        new_node = Node(value)
        if self.length == 0:
            self.head = new_node
            self.tail = new_node
            self.length += 1
            return
        new_node.next = self.head
        self.head = new_node
        self.length += 1


    def __contains__(self,value):
        temp_node = self.head
        while temp_node.next is not None:
            if temp_node.data == value:
                return True
            else:
                temp_node = temp_node.next
        
        return False
    
    
    def add_last(self,value):
        new_node = Node(value)
        if self.length == 0:
            self.add_first(value)
            return
        last_node = self.tail
        last_node.next = new_node
        self.tail = new_node
        self.length += 1
        
        
    def add_pos(self,value,position):
        new_node = Node(value)
        current_position = 0
        current_node = self.head
        
        if position == 0:
            self.add_first(value)
            return
        if position > self.length:
            print("InvalidPosition")
            return 
        if position == self.length:
            self.add_last(value)
            return
        
        while current_position + 1 != position:
            current_node = current_node.next
            current_position += 1
        
        temp_reference = current_node.next
        current_node.next = new_node
        new_node.next  = temp_reference
        
    
    def remove(self,position):
        current_node = self.head
        current_postion = 0
        
        if position == 0:
            self.head = self.head.next
            self.count -= 1
        elif position > self.length:
            print("InvalidPosition")
        else:
            while current_postion + 1 != position:
                current_node = current_node.next
                current_postion += 1
        
        current_node.next = current_node.next.next
        self.count -= 1
        
    def reverse(self):
        current_node = self.head
        prev_node = None
        while current_node is not None:
            next_node = current_node.next
            current_node.next = prev_node
            prev_node = current_node
            current_node = next_node        
        self.head = prev_node
        
    ''' Print and reverse print using recursion '''
    
    def Print(self,current_node):
        if current_node is None:
            print('None')
            return
        print(current_node.data,end = ' -> ')
        self.Print(current_node.next)
        
        
       
    def PrintRever(self,current_node):
        if current_node is None:
            return
        self.PrintRever(current_node.next)
        print(current_node.data,end = ' ')


'''
 Reverse Linked List in recursive way 

    def reverseUtil(self, curr, prev): 
		
		# If last node mark it head 
        if curr.next is None : 
            self.head = curr 
			
			# Update next to prev node 
            curr.next = prev 
            return
		
		# Save curr.next node for recursive call 
        next = curr.next

		# And update next 
        curr.next = prev 
	
        self.reverseUtil(next, curr) 


	# This function mainly calls reverseUtil() 
	# with previous as None 
    def reverseit(self): 
        #If condition to handle empty Linked List
        if self.head is not None: 
            return self.reverseUtil(self.head, None) 

'''

l = LinkedList()
l.add_first('Monday')
l.add_first('Sunday')
l.add_last('Tuesday')
l.add_pos('Friday', 3)
l.add_last('Saturday')
print(l)
#l.reverse()
#print(l)
l.PrintRever(l.head)