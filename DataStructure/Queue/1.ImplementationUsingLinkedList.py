# -*- coding: utf-8 -*-
"""
Created on Sat May  2 23:03:34 2020

@author: rabindra
"""


#If we add tail on linked list then insertion can be in O(1) as we don't require to travese list to reach end element
#Deletion can be done from head of linked list in O(1)
#So, adding one extra attribute to LinkedList makes insertion and deletion in O(1)

class Node:
    def __init__(self,value):
        self.data = value
        self.next = None
        
class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.count = 0       #Useful if calculation is length has to be done often
        
    def enque(self,data):
        new_node = Node(data)
        if self.head is None:
            self.head = new_node
            self.tail = new_node
            self.count += 1
            return
        last_node = self.tail
        last_node.next = new_node
        self.tail = new_node
        self.count += 1
    
    def deque(self):
        first_node_data = self.head.data
        self.head = self.head.next
        return first_node_data
        
    def front(self):
        return self.head.data
    
    def __repr__(self):
        current_node = self.head
        data_list = []
        while current_node is not None:
            data_list.append(str(current_node.data))
            current_node = current_node.next
        return 'exit <- '+' '.join(data_list) + ' <- entry'
    
l = LinkedList()
l.enque(5)
print(l)
l.enque(2)
print(l)
l.enque(3)
print(l)
print(l.front())
print(l.deque())
print(l)