# -*- coding: utf-8 -*-
"""
Created on Sat May  9 17:26:26 2020

@author: rabindra
"""
from queue import Queue

#Initializing
q1 = Queue()

#Adding Element to queue
q1.put('a')
q1.put('b')
q1.put('c')

#Getting element to deque
print(q1.get())
print(q1.get())
print(q1.get())

# Initializing a queue with fix size
q = Queue(maxsize = 3) 

#Get Queue Size
print(q.qsize())

# Return Boolean for Full  
print(q.full())  

# Return Boolean for Empty  
print(q.empty()) 