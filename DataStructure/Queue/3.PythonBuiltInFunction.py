# -*- coding: utf-8 -*-
"""
Created on Sat May  9 17:00:43 2020

@author: rabindra
"""
#Making UserDefined Queue

def enque(queue,data):
    queue.append(data)

def deque(queue):
    if len(queue) == 0:
        return "Empty queue"
    return queue.pop(0)

q1 = []
q2 = []
enque(q1,4)
enque(q1,7)
print(q1)
print(deque(q1))
print(q1)
print(deque(q1))
print(q1)
print(deque(q1))