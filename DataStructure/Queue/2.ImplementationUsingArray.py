# -*- coding: utf-8 -*-
"""
Created on Sat May  9 14:10:38 2020

@author: rabindra
"""


''' Concepts

1. We can have two variable rear and front to mark end and start of index
2. On queuing we increment rear by one and insert value at that place.
3. On dequeuing we take value from front and increase value by 1.
4. On queuing and dequing rear element may be at last and position prior to front may be
   vaccant. In this case on linear array it shows list is full but it isn't in actual. So
   using circular list may be solution i.e next element = (i + 1) % N instead of i + 1 makes
   array circular
'''

class Queue:
    
    queue_size = 10
    def __init__(self):
        self.first_index = -1
        self.last_index = -1
        self.data = [None] * Queue.queue_size   #Queue if size 10 is defined
        
    def enque(self,data):
        next_position = (self.last_index + 1 ) % Queue.queue_size  #10 is size of queue. Modulo makes list circular
        if next_position == self.first_index:
            print("Queue is full")
            return
        if self.first_index == -1:   #empty condition at first
            self.first_index = 0
            
        self.data[next_position] = data
        self.last_index = next_position
        
    def deque(self):
        if self.first_index == -1:
            return "Queue is empty"
        queue_value = self.data[self.first_index]
        if self.first_index == self.last_index: #ForLastDequeMakeQueueEmpty
            self.first_index = self.last_index = -1
            return queue_value
        self.first_index = (self.first_index + 1) % Queue.queue_size
        return queue_value
    
    def __repr__(self):
        data_to_return = []
        if self.first_index == -1:
            return "Empty Queue"
        start_pos = self.first_index
        while start_pos != self.last_index:
            data_to_return.append(str(self.data[start_pos]))
            start_pos = (start_pos + 1) % Queue.queue_size
        data_to_return.append(str(self.data[start_pos]))
        return '<- '.join(data_to_return)
    
q = Queue()
print("Enquing")
for i in range(7):
    q.enque(i+1)
    print(q)

print("Dequing")
print(q)

for i in range(5):
    print(q.deque())

print(q)

for i in range(9):
    q.enque(56+i)
    print(q)
    
for i in range(13):
    print(q.deque())
for i in range(4):
    q.enque(i+1)
    print(q)