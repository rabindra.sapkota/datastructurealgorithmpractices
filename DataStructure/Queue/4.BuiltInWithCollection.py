# -*- coding: utf-8 -*-
"""
Created on Sat May  9 17:20:38 2020

@author: rabindra
"""
from collections import deque

#Initializing Queue
q = deque()

#Adding element to queue
q.append('a')
q.append(2)
q.append('c')
print(q)

#Removing element from queue
print(q.popleft())
print(q.popleft())
print(q)