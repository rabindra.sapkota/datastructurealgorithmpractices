""" Milan ko solution
1. Write a function to return weight of lowercase english alphabets
in strength of k. Input parameter to function is a character and
output parameter is weight.

Example:
a. if k=4 then a=0,b=1,c=2,d=3,e=0,f=1,g=2,h=3,i=0...... and so on
i.e if we call function with h as input parameter it has to return 3
b. if k=3 then a=0,b=1,c=2,d=0,e=1,f=2,g=0,h=1,i=2...... and so on
i.e if we call function with h as input parameter it has to return 1
"""


#LinearComplexity
def get_weight(alphabet):
	weight_of_alphabet = ( ord(alphabet) + offset_value ) % strength_in_k
	return weight_of_alphabet


strength_in_k = int(input("Enter value of weight : "))
offset_value = strength_in_k - ( 97 % strength_in_k )	#97 is ascii value of a

'''
Logic:
1. Since value of strength has to be in range 0 to k-1. It can be achieved by dividing ascii value of character by k
2. Strength of a has to be 0 which mayn't be always be case of arbitary k. So offset_value has to be set such that it will be zero when dividing by any arbitary k
3. offset_value is nearest integer which when added to acsii value of a (97) makes it perfectly divisible by k
4. To rotate weight  % operator is used.
'''



#MilanKoConcept
"""
k = int(input("Enter strength: "))


# add the letters with index of strength to dictionary
def compose_dict():
    alphabets = "abcdefghijklmnopqrstuvwxyz"
    i = 0
    dictionary = {}
    for index in range(0, len(alphabets)):
        i = (i < k and i) or 0	     #When i gets greater than k-1 then it is reset back to 0. (core logic)
        dictionary.update({alphabets[index]: i})
        i += 1
    return dictionary


alphadict = compose_dict()

char = input("Enter the alphabet: ")
#  fetch the value from dictionary
print(alphadict.get(char))



Concept :  And also on basis of and /or

a and b = b if a != 0 and b != 0 else 0
a or b = a if a != 0 else b

It has been used to rotate weight logic is wow
"""
