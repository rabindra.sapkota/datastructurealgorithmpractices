# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 11:44:57 2020

@author: rabindra
"""
"""
Given an array of strings, group anagram together.

Example: ["eat","tea","tan","ate","nat","bat"] 

Output:
    [  ["ate","eat","tea"],
      ["nat","tan"],
      ["bat"]
    ]
    
"""    
def anagramAggregator(user_list):
    
    value_dictionary = {}
    
    for user_word in user_list:
        word_key = ''.join(sorted(user_word))
    
        if word_key in value_dictionary.keys():
            current_value = value_dictionary[word_key]
            current_value.append(user_word)
            value_dictionary[word_key] = current_value
       
        else:
            value_dictionary[word_key] = list(user_word.split())
    
    anagrams =  list(value_dictionary.values())
    return anagrams


user_list = ["eat","tea","tan","ate","nat","bat"]   
print(anagramAggregator(user_list)) 

#Complexity 3 as per lized.ws