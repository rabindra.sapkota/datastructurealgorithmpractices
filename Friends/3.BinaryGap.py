# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 19:22:23 2020

@author: rabindra
"""

"""
3. A binary gap within a positive integer N is any maximal sequence of 
consecutive zero surrounded by 1 at both sides. Write a function to find 
binary gap  def bingap(N) which returns binary gap of a number

eg: if n = 3 then its 101 in binay and gap is 1.

Using xor will be even more faster if possible
"""

import re

def solution(N):
    
    
    number = bin(N)
    
    '''Regex is used to find zeros surrounded by 1. all matches will
    be returned as a list '''
    
    matchingnumbers = re.findall(r'(?<=1)(0+)(?=1)',number)
    
    '''If list is empty there is no match so binary gap is zero
    If List is not empty then there is match so take largest element 
    of list on the basis of length of element of list. length of this element
    is the binary gap'''
    
    if len(matchingnumbers) == 0:
        return 0
    
    else:
        longestnumber = max(matchingnumbers,key=len)
        return len(longestnumber)


result = solution(9)
print(result)

#From lizard.ws  complexity = 2


"""
Milan ko concept


n = int(input("Enter number: "))

binaryValue = bin(n)
print(binaryValue)

binaryGap = 0
tempGap = 0
# First two index will be the type such as 0b
for i in range(2, len(binaryValue)):
    if int(binaryValue[i]) == 0:
        tempGap += 1
    else:
        if tempGap >= binaryGap:
            binaryGap = tempGap
        tempGap = 0

print(binaryGap)


"""
