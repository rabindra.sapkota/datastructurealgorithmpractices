# -*- coding: utf-8 -*-
"""
Created on Sat Apr 19 06:07:06 2020

@author: rabindra
"""


"""

A non empty array A consists N integer. The array contain odd number of 
elements and each can be paired with another that has same value except 
for one element. Find function to return unpaired element. 
Example if A = [9,3,9,3,9,7,9] function has to return 7 as it is unpaired.

"""

def solution(A):
    
    length = len(A)
    
    #If list is of single element then element is unpaired one
    if length == 1:
        c = A[0]
    else:
        #Sorting is done so that we can compare adjacent element in list
        #Without sort first pair of element can be at first place and last at last
        #So,if not sorted we have to travel whole list for every element i.e O(N^2)
        A.sort()
        
        for i in range(0,length - 1 , 2):
            if A[i] != A[i+1]:
                c = A[i]
                #Since sorted first place element has to match second one
                #third has to match fourth and so on. If not matched then its the solution
                break
    
    return c

#Complexity is O(N) + O(sort_complexity_used_by_function)
#i.e O(N + sort_complexity)
# By lizard.ws it is 4
    
print(solution([1,2,2,1,5,8,7,8,5]))


"""
Kamal ko concept

def my_odd_func(array_list):
    loop_count = len(array_list)
    set_list = set(array_list)

    my_odd_value = []  # To store odd pair values
    count = 0  # To count the number of occurance
    for setitem in set_list:
        for i in range(0, loop_count):
            if setitem == array_list[i]:
                count = count + 1

        if count % 2 == 0:
            pass
        else:
            my_odd_value.append(setitem)
        count = 0

    return my_odd_value


my_odd_pairs = my_odd_func([9, 3, 9, 5, 5])
print(my_odd_pairs)


"""