# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 23:17:18 2020

@author: rabindra
"""


'''
John works at clothing center. He has a large pile 
of sicks that he must pair by color for sale. Given an 
array of integer representing color of each sock, determine
how many pair of matching colors there are. Example there are
n = 7 socks with color ar = [1,2,1,2,1,3,2] . here is one pair
of color 1 and one pair of color 2. There are 3 odd socks left
so to total pair is 2
'''


#Modified Linear using set as hash table
def calcpair(sock_list):
    paired_socks = 0
    hash_table = set()
    
    for sock in sock_list:
        if sock in hash_table:
            paired_socks += 1
            hash_table.discard(sock)
        else:
            hash_table.add(sock)
        
    return paired_socks

n = list(map(int,input("Enter socks separated by comma: ").split(',')))
print (calcpair(n))






"""

#ModifiedSolution where sorting reduced complexity O(nlogn)
def calcpair(n):
    paired_socks = 0
    
    n.sort()
    length = len(n)
    i = 0
    
    while i < length:
        if i == length - 1:
            i = i + 1
        elif n[i] == n[i+1]:
            paired_socks += 1
            i = i + 2
        else:
            i = i + 1
    
    return(paired_socks)

n = list(map(int,input("Enter socks separated by comma: ").split(',')))
print (calcpair(n))

"""

"""
def calcpair(n):
    pairdict = {}
    c = 0
    for i in range(n):
        sock=input("Enter Socks color: ")
        
        if sock in pairdict.keys():
            pairdict[sock]+= 1

        else:
            pairdict[sock] = 1
            
        c = c + 1
        
    pairedsocks = 0
    
    for socks in pairdict.values():
        pairedsocks = pairedsocks + socks // 2
        
      #  global c
        c += 1
    
    return pairedsocks
 
n=int(input("Enter No of socks : "))
print (calcpair(n))


"""



"""
Milan ko concept


n = int(input("Enter number of shocks: "))

print("Enter the shocks ")
shocks = []
pairCount = 0
for i in range(0, n):
    shock = input()
    # check the new shock with the already added shocks
    for j in range(0, len(shocks)):
        # if new shock pair with previous shocks then increase the pair count, donot add
        # that shock to list and remove the matched shock from list. Also there is no need
        # to check further if input shocks matches the previous i.e. break
        if shock == shocks[j]:
            pairCount += 1
            shocks.pop(j)
            break
    # if the new shock don't pair with existing shocks then add that new shock to list
    # so that it will be checked will new shocks later through input.
    else:
        shocks.append(shock)

print("Total Pair is: ", pairCount)


New : Concept of append and pop and increase counter  Wooow
User of for else with break statement. Woow  (else executes if loop executes normally and doesn't encounter break')
"""