# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 17:32:10 2020

@author: rabindra
"""

import random

def random_character_generator():
    random_character = chr(int(97 + (105-97)*random.random()))
    return (random_character)


random_length = int(3 + 8*random.random())


test_list = []


for i in range(1000000):
    string = ''
    random_length = int(3 + 8*random.random())
    for i in range(random_length):
        string += random_character_generator()
    
    test_list.append(string)
    
    
"""  
def anagramAggregator(user_list):
    value_dictionary = {}
    
    for user_word in user_list:
        word_key = ''.join(sorted(user_word))
    
        if word_key in value_dictionary.keys():
            current_value = value_dictionary[word_key]
            current_value.append(user_word)
            value_dictionary[word_key] = current_value
       
        else:
            value_dictionary[word_key] = list(user_word.split())
    
    anagrams =  list(value_dictionary.values())
    return anagrams


#user_list = ["eat","tea","tan","ate","nat","bat"]   
print(anagramAggregator(test_list))
"""